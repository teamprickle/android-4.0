package com.prickle4.adapters;

import android.content.Context;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.prickle4.R;

import java.util.ArrayList;

/**
 * Created by Drake Lee on 4/6/2016.
 */
public class ArrayAdapterPlaceItem extends RecyclerView.Adapter<ArrayAdapterPlaceItem.ViewHolder> {
    Context mContext;
    public ArrayList<PlaceItem> data = null;

    public ArrayAdapterPlaceItem(Context mContext, ArrayList<PlaceItem> data) {
        this.mContext = mContext;
        this.data = data;
    }

    @Override
    public ArrayAdapterPlaceItem.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.place_item, null);
        ViewHolder vh = new ViewHolder(itemLayoutView);
        vh.setIsRecyclable(false);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        if (data.get(position) != null) {
            viewHolder.txtPlaceName.setText(data.get(position).getName());
        }
    }

    @Override
    public int getItemCount() { return data.size(); }

    public void addItem(PlaceItem place) {
        data.add(place);
        dataSetChanged(data.size()-1);
    }

    public PlaceItem getItem(int position) {
        return data.get(position);
    }

    @UiThread
    protected void dataSetChanged(int position) {
        notifyItemInserted(position);
    }

    @UiThread
    protected void dataRemoved(int position) {
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, data.size());
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView txtPlaceName;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            txtPlaceName = (TextView) itemLayoutView.findViewById(R.id.place_name);
            itemLayoutView.setVisibility(View.VISIBLE);
            itemLayoutView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            v.setVisibility(View.GONE);
            remove(getAdapterPosition());
        }
    }

    public void remove(int position) {
        data.remove(position);
        dataRemoved(position);
    }
}
