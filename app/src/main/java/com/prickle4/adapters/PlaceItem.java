package com.prickle4.adapters;

/**
 * Created by Drake Lee on 4/6/2016.
 */
public class PlaceItem {
    private String name;
    private double lat;
    private double lng;

    public PlaceItem(String name, double lat, double lng) {
        this.name = name;
        this.lat = lat;
        this.lng = lng;
    }

    public String getName() {
        return name;
    }

    public double getLat() { return lat; }

    public double getLng() { return lng; }
}
