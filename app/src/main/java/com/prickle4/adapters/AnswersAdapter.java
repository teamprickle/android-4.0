package com.prickle4.adapters;

import android.content.Context;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.prickle4.R;
import com.prickle4.helpers.AnswerVoteOnClickListener;
import com.prickle4.helpers.CustomRequest;
import com.prickle4.static_class.Constants;
import com.prickle4.static_class.DateTimeUtils;
import com.prickle4.static_class.KeyValueDB;
import com.prickle4.static_class.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by Drake Lee on 3/31/2016.
 */
public class AnswersAdapter extends RecyclerView.Adapter<AnswersAdapter.ViewHolder> {

    private Context mContext;
    private AnswerData[] answerData;

    public AnswersAdapter(AnswerData[] answerData, Context context) {
        this.answerData = answerData;
        mContext = context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public AnswersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                        int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.answer_card, null);

        // create ViewHolder
        return new ViewHolder(itemLayoutView);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData
        if (answerData[position] != null) {
            viewHolder.txtUsername.setText(answerData[position].getUsername());
            viewHolder.txtAnswer.setText(answerData[position].getAnswer());
            viewHolder.txtCreated.setText(DateTimeUtils.formattedTimeFromMongoDB(answerData[position].getCreated(), mContext));
            viewHolder.txtVotes.setText(String.valueOf(answerData[position].getVotes()));
            viewHolder.imgDownVote.setOnClickListener(new AnswerVoteOnClickListener(answerData[position].getAnswerId(), mContext));
            viewHolder.imgUpVote.setOnClickListener(new AnswerVoteOnClickListener(answerData[position].getAnswerId(), mContext));

        }
    }


    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtUsername;
        public TextView txtAnswer;
        public TextView txtCreated;
        public TextView txtVotes;
        public ImageView imgUpVote;
        public ImageView imgDownVote;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            txtUsername = (TextView) itemLayoutView.findViewById(R.id.card_answer_username);
            txtAnswer = (TextView) itemLayoutView.findViewById(R.id.card_answer_text);
            txtCreated = (TextView) itemLayoutView.findViewById(R.id.card_answer_created);
            txtVotes = (TextView) itemLayoutView.findViewById(R.id.answer_card_vote);
            imgUpVote = (ImageView) itemLayoutView.findViewById(R.id.upvote_answer_card_button);
            imgDownVote = (ImageView) itemLayoutView.findViewById(R.id.downvote_answer_card_button);
        }

    }

    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return answerData.length;
    }

}
