package com.prickle4.adapters;

/**
 * Created by Drake Lee on 3/31/2016.
 */
public class AnswerData {
    private String answerId;
    private String username;
    private String answer;
    private String created;
    private int votes;

    public AnswerData(String answerId, String username, String answer, String created, int votes) {
        this.answerId = answerId;
        this.username = username;
        this.answer = answer;
        this.created = created;
        this.votes = votes;
    }

    public String getAnswerId() { return answerId; }

    public String getUsername() {
        return username;
    }

    public String getAnswer() {
        return answer;
    }

    public String getCreated() {
        return created;
    }

    public int getVotes() {
        return votes;
    }

}
