package com.prickle4.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.prickle4.R;
import com.prickle4.helpers.CustomRequest;
import com.prickle4.static_class.Constants;
import com.prickle4.static_class.KeyValueDB;
import com.prickle4.static_class.Utility;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

public class ChangePasswordActivity extends AppCompatActivity {

    TextView textCurrentPassword;
    TextView textConfirmPassword;
    TextView textNewPassword;
    EditText editCurrentPassword;
    EditText editConfirmPassword;
    EditText editNewPassword;
    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            checkFieldsForEmptyValues();
        }

        @Override
        public void afterTextChanged(Editable editable) {
        }

    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        textCurrentPassword = (TextView) findViewById(R.id.textCurrentPassword);
        textConfirmPassword = (TextView) findViewById(R.id.textConfirmPassword);
        textNewPassword = (TextView) findViewById(R.id.textNewPassword);
        editCurrentPassword = (EditText) findViewById(R.id.editCurrentPassword);
        editConfirmPassword = (EditText) findViewById(R.id.editConfirmPassword);
        editNewPassword = (EditText) findViewById(R.id.editNewPassword);
        editCurrentPassword.addTextChangedListener(textWatcher);
        editConfirmPassword.addTextChangedListener(textWatcher);
        editNewPassword.addTextChangedListener(textWatcher);
        checkFieldsForEmptyValues();
    }

    public void toggleText(TextView text, String s) {
        if (s.length() > 0) {
            text.setVisibility(View.VISIBLE);
        } else {
            text.setVisibility(View.GONE);
        }
    }

    private void checkFieldsForEmptyValues() {
        String currentPassword = editCurrentPassword.getText().toString();
        String confirmPassword = editConfirmPassword.getText().toString();
        String newPassword = editNewPassword.getText().toString();

        toggleText(textCurrentPassword, currentPassword);
        toggleText(textConfirmPassword, confirmPassword);
        toggleText(textNewPassword, newPassword);
    }

    private static final int SUCCESS = 0;
    private static final int ERROR_HASHING = 1;
    private static final int ERROR_WRONG_PASSWORD = 2;
    private static final int ERROR_CONFIRM_PASSWORD = 3;

    private int updatePassword(String currentPassword, String confirmPassword, final String newPassword) {
        try {
            String hashedCurrent = Utility.SHA1(currentPassword);

            if (hashedCurrent.equals(KeyValueDB.get(this, "pricklePassword"))) {
                if (confirmPassword.equals(newPassword)) {
                    Map<String, String> params = new HashMap<String, String>();
                    final String hashedNew = Utility.SHA1(newPassword);
                    params.put("password", hashedNew);
                    CustomRequest jsRequest = new CustomRequest(Request.Method.PUT, Constants.UPDATE_PROFILE_URL, params,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    System.out.println(response);
                                    KeyValueDB.set(getApplicationContext(), "pricklePassword", hashedNew);
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(getApplicationContext(), "Error sending new password!", Toast.LENGTH_LONG).show();
                            System.out.println("Error in password PUT.");
                            error.printStackTrace();
                        }
                    }, this);

                    Volley.newRequestQueue(getApplicationContext()).add(jsRequest);

                    return SUCCESS;
                }
                return ERROR_CONFIRM_PASSWORD;
            }
            return ERROR_WRONG_PASSWORD;
        } catch (Exception e) {
            return ERROR_HASHING;
        }
    }

    public void cancel(View view) {
        finish();
    }

    public void save(View view) {
        String currentPassword = editCurrentPassword.getText().toString();
        String confirmPassword = editConfirmPassword.getText().toString();
        String newPassword = editNewPassword.getText().toString();
        int result = updatePassword(currentPassword, confirmPassword, newPassword);
        switch (result) {
            case SUCCESS:
                Toast.makeText(getApplicationContext(), "Password updated!", Toast.LENGTH_LONG).show();
                finish();
                break;
            case ERROR_HASHING:
                Toast.makeText(getApplicationContext(), "Error hashing password, try a different new password.", Toast.LENGTH_LONG).show();
                break;
            case ERROR_WRONG_PASSWORD:
                Toast.makeText(getApplicationContext(), "Please check that the current password is correct.", Toast.LENGTH_LONG).show();
                break;
            case ERROR_CONFIRM_PASSWORD:
                Toast.makeText(getApplicationContext(), "Please check that the new password matches.", Toast.LENGTH_LONG).show();
                break;
        }
    }
}
