package com.prickle4.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.prickle4.R;
import com.prickle4.activities.question.CreateQuestionActivity;
import com.prickle4.fragments.ExploreFragment;
import com.prickle4.fragments.MainPagerFragment;
import com.prickle4.fragments.QuestionListFragment;
import com.prickle4.fragments.question_content.QuestionContent;
import com.prickle4.helpers.notifications.NotificationEventReceiver;
import com.prickle4.static_class.KeyValueDB;
import com.prickle4.static_class.Utility;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        QuestionListFragment.OnListFragmentInteractionListener,
        MainPagerFragment.OnFragmentInteractionListener,
        ExploreFragment.OnFragmentInteractionListener {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private NavigationView mNavigationView;
    MainPagerFragment mainPagerFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mainPagerFragment = new MainPagerFragment();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, mainPagerFragment)
                .commit();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, CreateQuestionActivity.class);
                startActivity(intent);
            }
        });

        KeyValueDB.set(getApplicationContext(), "lastNewQuestionFound", System.currentTimeMillis());
        KeyValueDB.set(getApplicationContext(), "lastMyQuestionAnswered", System.currentTimeMillis());

        if("ERROR".equals(KeyValueDB.get(getApplicationContext(), "newQuestionNotificationOn"))){
            KeyValueDB.set(getApplicationContext(), "newQuestionNotificationOn", "on");
            KeyValueDB.set(getApplicationContext(), "newAnswerNotificationOn", "on");
        }
        NotificationEventReceiver.stopNewQuestionAlarm(getApplicationContext());
        NotificationEventReceiver.stopNewQuestionAlarm(getApplicationContext());

        if("on".equals(KeyValueDB.get(getApplicationContext(), "newQuestionNotificationOn"))){
            NotificationEventReceiver.setupNewQuestionAlarm(getApplicationContext());
        }

        if("on".equals(KeyValueDB.get(getApplicationContext(), "newAnswerNotificationOn"))){
            NotificationEventReceiver.setupNewAnswerAlarm(getApplicationContext());
        }

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        //Set user profile picture and username
        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(this);
        View header = mNavigationView.getHeaderView(0);

        TextView headerUsername = (TextView) header.findViewById(R.id.headerUsername);
        headerUsername.setText(KeyValueDB.get(getApplicationContext(), "prickleUsername"));
        ImageView headerProfile = (ImageView) header.findViewById(R.id.headerProfileImage);
        String avatarBase64 = KeyValueDB.get(getApplicationContext(), "userAvatar");
        if (avatarBase64 != "ERROR") {
            headerProfile.setImageBitmap(Utility.decodeBase64(avatarBase64));
        }
        header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent profileIntent = new Intent(HomeActivity.this, ProfileActivity.class);
                mDrawerLayout.closeDrawer(GravityCompat.START);
                startActivity(profileIntent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Utility.unbindDrawables(findViewById(R.id.drawer_layout));
        System.gc();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.action_bar_navigation_drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_home) {
            HomeActivity.this.getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_frame, mainPagerFragment, null)
                    .addToBackStack(null)
                    .commit();
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else if (id == R.id.nav_favorites) {

        } else if (id == R.id.nav_statistics) {

        }  else if (id == R.id.nav_settings) {
            Intent settingsIntent = new Intent(getApplicationContext(), SettingsActivity.class);
            startActivity(settingsIntent);
        }

        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onListFragmentInteraction(QuestionContent.Question item) {
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
    }

    public void clickBottomBar(View view) {
        mainPagerFragment.clickBottomBar(view);
    }

}
