package com.prickle4.activities;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.prickle4.R;
import com.prickle4.helpers.CustomRequest;
import com.prickle4.static_class.Constants;
import com.prickle4.static_class.KeyValueDB;
import com.prickle4.static_class.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class EditProfileActivity extends AppCompatActivity {

    ImageView avatar;
    Bitmap bitmapAvatar;
    boolean avatarChanged = false;
    String avatarBase64;
    TextView textUsername;
    TextView textFirstName;
    TextView textLastName;
    TextView textBio;
    EditText editFirstName;
    EditText editLastName;
    EditText editBio;
    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            checkFieldsForEmptyValues();
        }

        @Override
        public void afterTextChanged(Editable editable) {
        }

    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        avatar = (ImageView) findViewById(R.id.avatar);
        textUsername = (TextView) findViewById(R.id.textUsername);
        textFirstName = (TextView) findViewById(R.id.textFirstName);
        textLastName = (TextView) findViewById(R.id.textLastName);
        textBio = (TextView) findViewById(R.id.textBio);
        editFirstName = (EditText) findViewById(R.id.editFirstName);
        editLastName = (EditText) findViewById(R.id.editLastName);
        editBio = (EditText) findViewById(R.id.editBio);
        editFirstName.addTextChangedListener(textWatcher);
        editLastName.addTextChangedListener(textWatcher);
        editBio.addTextChangedListener(textWatcher);
        getProfileInformation();
        checkFieldsForEmptyValues();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Utility.unbindDrawables(findViewById(R.id.editProfileRoot));
        System.gc();
    }

    private int PICK_IMAGE_REQUEST = 1;

    public void setImage(View view) {
        Intent intent = new Intent();
        // Show only images, no videos or anything else
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        // Always show the chooser (if there are multiple options available)
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();

            try {
                bitmapAvatar = Utility.getCircularBitmap(MediaStore.Images.Media.getBitmap(getContentResolver(), uri));

                ImageView imageView = (ImageView) findViewById(R.id.avatar);
                imageView.setImageBitmap(bitmapAvatar);
                avatarBase64 = Utility.encodeToBase64(bitmapAvatar, Bitmap.CompressFormat.PNG, 100);
                avatarChanged = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void cancel(View view) {
        finish();
    }

    public void getProfileInformation() {
        textUsername.setText(KeyValueDB.get(getApplicationContext(), "prickleUsername"));

        String bio = KeyValueDB.get(getApplicationContext(), "userDescription");
        if(bio != "ERROR"){
            editBio.setText(bio);
        }
        String firstName = KeyValueDB.get(getApplicationContext(), "userFirstName");
        if(firstName != "ERROR"){
            editFirstName.setText(firstName);
        }
        String lastName = KeyValueDB.get(getApplicationContext(), "userLastName");
        if(lastName != "ERROR") {
            editLastName.setText(lastName);
        }
        avatarBase64 = KeyValueDB.get(getApplicationContext(), "userAvatar");
        if(avatarBase64 != "ERROR"){
            avatar.setImageBitmap(Utility.decodeBase64(avatarBase64));
        }
    }

    public void save(View view) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("first_name", editFirstName.getText().toString());
        params.put("last_name", editLastName.getText().toString());
        params.put("description", editBio.getText().toString());
        if (avatarChanged) {
            params.put("avatar", avatarBase64);
        }
        CustomRequest jsRequest = new CustomRequest(Request.Method.PUT, Constants.UPDATE_PROFILE_URL, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText(getApplicationContext(), "Profile updated!", Toast.LENGTH_LONG).show();

                        //Update shared preferences if successful
                        KeyValueDB.set(getApplicationContext(), "userFirstName", editFirstName.getText().toString());
                        KeyValueDB.set(getApplicationContext(), "userLastName", editLastName.getText().toString());
                        KeyValueDB.set(getApplicationContext(), "userDescription", editBio.getText().toString());
                        if (avatarChanged) {
                            KeyValueDB.set(getApplicationContext(), "userAvatar", avatarBase64);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Error sending profile information!", Toast.LENGTH_LONG).show();
                System.out.println("Error in profile PUT.");
                error.printStackTrace();
            }
        }, this);

        Volley.newRequestQueue(getApplicationContext()).add(jsRequest);

        finish();
    }

    public void toggleText(TextView text, String s) {
        if (s.length() > 0) {
            text.setVisibility(View.VISIBLE);
        } else {
            text.setVisibility(View.GONE);
        }
    }

    private void checkFieldsForEmptyValues() {
        String sFirstName = editFirstName.getText().toString();
        String sLastName = editLastName.getText().toString();
        String sBio = editBio.getText().toString();

        toggleText(textFirstName, sFirstName);
        toggleText(textLastName, sLastName);
        toggleText(textBio, sBio);
    }
}
