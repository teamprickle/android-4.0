package com.prickle4.activities;

/**
 * Created by Drake Lee on 3/25/2016.
 */

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.prickle4.R;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.prickle4.helpers.CustomRequest;
import com.prickle4.static_class.Constants;
import com.prickle4.static_class.KeyValueDB;
import com.prickle4.static_class.Utility;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;


public class LoginActivity extends Activity {
    ProgressDialog prgDialog;
    EditText userET;
    EditText pwdET;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        userET = (EditText)findViewById(R.id.login_user);
        pwdET = (EditText)findViewById(R.id.login_pwd);
        prgDialog = new ProgressDialog(this);
        prgDialog.setMessage("Please wait...");
        prgDialog.setCancelable(false);

        // Check for saved credentials
        String username = KeyValueDB.get(this, "prickleUsername");
        String password = KeyValueDB.get(this, "pricklePassword");
        String userId   = KeyValueDB.get(this, "prickleUserId");
        //eraseCredentials(); // TODO: ERASE THIS ONLY FOR TESTING

        if (!username.equals("ERROR") && !password.equals("ERROR") && !userId.equals("ERROR")){
            invokeWS(username, password);
        }
    }

    /**
     * Method gets triggered when activity_login is clicked
     *
     */
    public void loginUser(View view) {
        String username = userET.getText().toString();
        String password = pwdET.getText().toString();

        if(Utility.isNotNull(username) && Utility.isNotNull(password)) {
            String passwordHash="";
            try {
                passwordHash = Utility.SHA1(password);
            } catch (NoSuchAlgorithmException no) {

            } catch (UnsupportedEncodingException e) {

            }
            invokeWS(username, passwordHash);
        }
        else {
            Toast.makeText(getApplicationContext(), "Please fill the form", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Method that performs RESTful webservice invocations
     *
     * @param username, password
     */
    public void invokeWS(final String username, final String password) {
        prgDialog.show();

        Map<String, String> params = new HashMap<>();
        params.put("username", username);
        params.put("password", password);

        CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, Constants.LOGIN_URL, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        prgDialog.hide();
                        try {
                            if (response.get("statusCode").equals("200")) {
                                SharedPreferences settings = getSharedPreferences("PrickleUserInfo", 0);
                                SharedPreferences.Editor editor = settings.edit();
                                editor.putString("prickleUsername", username);
                                editor.putString("pricklePassword", password);
                                editor.putString("prickleUserId", (String) response.get("userId"));
                                editor.commit();
                                Toast.makeText(getApplicationContext(), "You are successfully logging in!", Toast.LENGTH_LONG).show();
                                prgDialog.dismiss();
                                navigatetoHomeActivity();
                            } else if (response.get("statusCode").equals("401")) {
                                Toast.makeText(getApplicationContext(), "Bad username or password!", Toast.LENGTH_LONG).show();
                                prgDialog.dismiss();
                            }
                        } catch (JSONException e) {

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                prgDialog.hide();
                Toast.makeText(getApplicationContext(), "Login Error!", Toast.LENGTH_LONG).show();
                System.out.println("Error in user activity_login.");
                error.printStackTrace();
            }
        });

        Volley.newRequestQueue(this).add(jsObjRequest);
    }

    /**
     * Method navigating from activity_login to home
     */
    public void navigatetoHomeActivity() {
        Intent homeIntent = new Intent(getApplicationContext(), HomeActivity.class);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(homeIntent);
    }

    /**
     * Method triggered when activity_register is clicked
     *
     * @param view
     */
    public void navigatetoRegisterActivity(View view) {
        Intent loginIntent = new Intent(getApplicationContext(), RegisterActivity.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(loginIntent);
    }

    public void eraseCredentials() {
        SharedPreferences settings = getSharedPreferences("PrickleUserInfo", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        editor.commit();
    }
}
