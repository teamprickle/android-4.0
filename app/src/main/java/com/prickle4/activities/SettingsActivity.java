package com.prickle4.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.prickle4.R;
import com.prickle4.static_class.KeyValueDB;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    public void signOut(View view){
        SharedPreferences settings = getSharedPreferences("PrickleUserInfo", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        editor.commit();
        Intent loginIntent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(loginIntent);
    }

    public void editProfile(View view){
        Intent editProfileIntent = new Intent(getApplicationContext(), EditProfileActivity.class);
        startActivity(editProfileIntent);
    }

    public void changePassword(View view){
        Intent changePasswordIntent = new Intent(getApplicationContext(), ChangePasswordActivity.class);
        startActivity(changePasswordIntent);
    }

    public void contentNotifications(View view){
        Intent contentNotificationsIntent = new Intent(getApplicationContext(), QuestionSettingsActivity.class);
        startActivity(contentNotificationsIntent);
    }

    public void changeFavorites(View view){
        Intent favoritesIntent = new Intent(getApplicationContext(), FavoritesSettingsActivity.class);
        startActivity(favoritesIntent);
    }

    public void aboutApp(View view){
        Intent aboutAppIntent = new Intent(getApplicationContext(), AboutActivity.class);
        startActivity(aboutAppIntent);
    }

    public void sendFeedback(View view){
        Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
        emailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        emailIntent.setType("vnd.android.cursor.item/email");
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[] {"wustl.drake@gmail.com"});
        StringBuilder titleBuilder = new StringBuilder("Prickle Feedback: ").append(getResources().getString(R.string.app_version));
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, titleBuilder.toString());
        StringBuilder contentBuilder = new StringBuilder("\n\n\n-----------------\n")
                .append("OS Version: ").append(System.getProperty("os.version")).append("\n")
                .append("SDK Version: ").append(Build.VERSION.SDK_INT).append("\n")
                .append("Device manufacturer: ").append(Build.MANUFACTURER).append("\n")
                .append("Device model: ").append(android.os.Build.MODEL).append("\n")
                .append("Username: ").append(KeyValueDB.get(getApplicationContext(), "prickleUsername"));
        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, contentBuilder.toString());
        startActivity(Intent.createChooser(emailIntent, "Send mail using..."));
    }
}
