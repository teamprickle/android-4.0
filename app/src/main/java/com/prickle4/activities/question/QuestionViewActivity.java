package com.prickle4.activities.question;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.prickle4.R;
import com.prickle4.adapters.AnswerData;
import com.prickle4.adapters.AnswersAdapter;
import com.prickle4.helpers.CustomArrayRequest;
import com.prickle4.helpers.CustomRequest;
import com.prickle4.helpers.GridSpacingDecoration;
import com.prickle4.static_class.Constants;
import com.prickle4.static_class.KeyValueDB;
import com.prickle4.static_class.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Drake Lee on 3/31/2016.
 */

public class QuestionViewActivity extends AppCompatActivity {
    private TextView questionTitle;
    private TextView questionDetails;
    private TextView questionLocation;
    private TextView viewsText;
    private TextView numberAnswersText;
    private TextView editQuestionBTN;
    private EditText answerET;
    private TextView answerBTN;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeContainer;
    private String questionId;

    private static final int GET_QUESTION_DETAIL = 0;
    private static final int POST_ANSWER = 1;
    private static final int GET_ANSWERS = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_view);

        questionTitle = (TextView) findViewById(R.id.card_question_title);
        questionDetails = (TextView) findViewById(R.id.card_question_details);
        questionLocation = (TextView) findViewById(R.id.questionViewLocation);
        viewsText = (TextView) findViewById(R.id.card_question_views);
        numberAnswersText = (TextView) findViewById(R.id.card_question_number_answers);
        editQuestionBTN = (TextView) findViewById(R.id.card_question_edit_button);
        answerET = (EditText) findViewById(R.id.card_question_answer_field);
        answerBTN = (TextView) findViewById(R.id.card_question_answer_button);

        answerBTN.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (((LinearLayout) findViewById(R.id.card_question_answer_area)).getVisibility() == View.VISIBLE)
                    ((LinearLayout) findViewById(R.id.card_question_answer_area)).setVisibility(View.GONE);
                else
                    ((LinearLayout) findViewById(R.id.card_question_answer_area)).setVisibility(View.VISIBLE);
                // Places Auto Complete ----
            }
        });

        Intent received = getIntent();
        questionId = received.getStringExtra("questionId");
        // Get data for question card
        invokeWS(GET_QUESTION_DETAIL);

        recyclerView = (RecyclerView) findViewById(R.id.answer_recycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new GridSpacingDecoration());
        recyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                invokeWS(GET_ANSWERS);
            }
        });

        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    /**
     * Method that performs RESTful webservice invocations
     *
     * @param
     */
    public void invokeWS(int service) {
        switch (service) {
            // Get the question details
            case GET_QUESTION_DETAIL: {
                Map<String, String> params = new HashMap<>();
                params.put("userId", KeyValueDB.get(this, "prickleUserId"));
                CustomRequest jsObjRequest = new CustomRequest(Request.Method.PUT, Constants.QUESTION_URL + "/" + questionId, params,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    questionTitle.setText((String) response.getJSONObject("question").get("title"));
                                    questionDetails.setText((String) response.getJSONObject("question").get("details"));
                                    questionLocation.setText(response.getJSONObject("question").getJSONObject("place").getString("name"));
                                    viewsText.setText(response.getJSONObject("question").get("views").toString() + " views");
                                    if (response.getBoolean("questionOwner")) {
                                        editQuestionBTN.setVisibility(View.VISIBLE);
                                    }
                                    // Get the answers for the question.
                                    invokeWS(GET_ANSWERS);
                                } catch (JSONException e) {

                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Question Error!", Toast.LENGTH_LONG).show();
                        System.out.println("Error in getting questions.");
                        error.printStackTrace();
                    }
                }, this);

                Volley.newRequestQueue(getApplicationContext()).add(jsObjRequest);
            }
            break;
            // Post an answer
            case POST_ANSWER: {
                Map<String, String> params = new HashMap<>();
                params.put("username", KeyValueDB.get(this, "prickleUsername"));
                params.put("userId", KeyValueDB.get(this, "prickleUserId"));
                params.put("answer", answerET.getText().toString());
                params.put("questionId", questionId);

                CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, Constants.ANSWER_URL, params,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    if (response.get("statusCode").equals("200")) {
                                        Toast.makeText(getApplicationContext(), "Answer posted!", Toast.LENGTH_LONG).show();
                                        answerET.setText("");
                                        recyclerView.getAdapter().notifyDataSetChanged();
                                        invokeWS(GET_ANSWERS);
                                    } else if (response.get("statusCode").equals("187")) {
                                        Toast.makeText(getApplicationContext(), "Can't post more than one answer!", Toast.LENGTH_LONG).show();
                                    }
                                } catch (JSONException e) {

                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Error posting answer!", Toast.LENGTH_LONG).show();
                        System.out.println("Error in answer POST.");
                        error.printStackTrace();
                    }
                }, this);

                Volley.newRequestQueue(getApplicationContext()).add(jsObjRequest);
            }
            break;
            // Get answers for question
            case GET_ANSWERS: {
                Map<String, String> params = new HashMap<>();
                params.put("questionId", questionId);

                CustomArrayRequest jsArrRequest = new CustomArrayRequest(Request.Method.GET, Constants.ANSWER_URL + "/" + questionId, params,
                        new Response.Listener<JSONArray>() {
                            @Override
                            public void onResponse(JSONArray response) {
                                try {
                                    AnswerData answerData[] = new AnswerData[response.length()];
                                    numberAnswersText.setText(((Integer) response.length()).toString() + " answers");
                                    for (int i = 0; i < response.length(); ++i) {
                                        JSONObject e = response.getJSONObject(i);
                                        answerData[i] = new AnswerData(e.getString("_id"), e.getString("username"), e.getString("answer"),
                                                e.getString("created"), e.getInt("votes"));
                                    }
                                    recyclerView.setAdapter(new AnswersAdapter(answerData, getApplicationContext()));
                                    recyclerView.getAdapter().notifyDataSetChanged();
                                    swipeContainer.setRefreshing(false);
                                } catch (JSONException e) {
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Error posting answer!", Toast.LENGTH_LONG).show();
                        System.out.println("Error in answer POST.");
                        error.printStackTrace();
                    }
                }, this);

                Volley.newRequestQueue(getApplicationContext()).add(jsArrRequest);
            }
            break;
        }
    }

    public void postAnswer(View view) {
        String answer = answerET.getText().toString();

        if (Utility.isNotNull(answer)) {
            invokeWS(POST_ANSWER);
        } else {
            Toast.makeText(getApplicationContext(), "Please fill out answer!", Toast.LENGTH_LONG).show();
        }
    }

}
