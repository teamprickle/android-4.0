package com.prickle4.activities.question;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.prickle4.R;
import com.prickle4.activities.HomeActivity;
import com.prickle4.activities.LoginActivity;
import com.prickle4.helpers.CustomRequest;
import com.prickle4.static_class.Constants;
import com.prickle4.static_class.KeyValueDB;
import com.prickle4.static_class.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Drake Lee on 3/26/2016.
 */
public class CreateQuestionActivity extends Activity {
    ProgressDialog prgDialog;
    TextView errorMsg;
    EditText titleET;
    EditText detailsET;
    double lat;
    double lng;
    String placeName;

    private static final String TAG = "CreateQuestion";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_question);

        errorMsg = (TextView) findViewById(R.id.create_question_error);
        titleET = (EditText) findViewById(R.id.create_question_title);
        detailsET = (EditText) findViewById(R.id.create_question_details);
        prgDialog = new ProgressDialog(this);
        prgDialog.setMessage("Please wait...");
        prgDialog.setCancelable(false);

        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.question_autocomplete_fragment);

        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_NONE)
                .build();

        autocompleteFragment.setFilter(typeFilter);

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // Add places here
                lat = place.getLatLng().latitude;
                lng = place.getLatLng().longitude;
                placeName = (String) place.getName();
                Log.i(TAG, "Place: " + place.getLatLng());
            }

            @Override
            public void onError(Status status) {
                Log.i(TAG, "An error occurred: " + status);
            }
        });
    }

    /**
     * Method gets triggered when activity_login is clicked
     *
     * @param view
     */
    public void createQuestion(View view) {
        String title = titleET.getText().toString();
        String details = detailsET.getText().toString();


        if(Utility.isNotNull(title) && Utility.isNotNull(details)) {
            String userId = KeyValueDB.get(this, "prickleUserId");
            if (!userId.equals("ERROR")) {
                invokeWS(title, details, userId);
            } else {
                prgDialog.dismiss();
                navigateToLoginActivity();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Please fill the form", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Method that performs RESTful webservice invocations
     *
     * @param title, details, userId
     */
    public void invokeWS(final String title, final String details, final String userId) {
        prgDialog.show();

        Map<String, String> params = new HashMap<String, String>();
        params.put("title", title);
        params.put("details", details);
        params.put("userId", userId);
        params.put("lng", "" + lng);
        params.put("lat", "" + lat);
        params.put("placeName", placeName);
        params.put("username", KeyValueDB.get(this, "prickleUsername"));

        CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, Constants.QUESTION_URL, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        prgDialog.hide();
                        try {
                            if (response.get("statusCode").equals("200")) {
                                Toast.makeText(getApplicationContext(), "You have submitted a question!", Toast.LENGTH_LONG).show();
                                prgDialog.dismiss();
                                navigatetoHomeActivity();
                            }
                        } catch (JSONException e) {

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                prgDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Can't post question!", Toast.LENGTH_LONG).show();
                System.out.println("Error in creating question.");
                error.printStackTrace();
            }
        }, this);

        Volley.newRequestQueue(this).add(jsObjRequest);
    }

    /**
     * Method navigating to activity_login
     */
    public void navigateToLoginActivity() {
        Intent loginIntent = new Intent(getApplicationContext(), LoginActivity.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(loginIntent);
    }

    /**
     * Method navigating from activity_login to home
     */
    public void navigatetoHomeActivity() {
        Intent homeIntent = new Intent(getApplicationContext(), HomeActivity.class);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(homeIntent);
    }
}
