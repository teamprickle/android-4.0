package com.prickle4.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import com.prickle4.R;
import com.prickle4.helpers.notifications.NotificationEventReceiver;
import com.prickle4.static_class.KeyValueDB;

public class QuestionSettingsActivity extends AppCompatActivity {


    public boolean newQuestionNotificationsOn = false;
    public boolean newAnswerNotificationsOn = false;
    public int radius = 10;
    public Switch switchNotificationsAnswered;
    public Switch switchNotificationsQuestions;
    public SeekBar seekBarQuestionsNearMe;
    public TextView textRadius;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        radius = KeyValueDB.getInt(getApplicationContext(), "nearMeRadius");
        textRadius = (TextView) findViewById(R.id.textRadius);
        textRadius.setText(String.valueOf(radius) + " mi");
        seekBarQuestionsNearMe = (SeekBar) findViewById(R.id.seekBarQuestionsNearMe);
        seekBarQuestionsNearMe.setProgress(radius/5);
        seekBarQuestionsNearMe.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                radius = 5 + progress*5;
                textRadius.setText(String.valueOf(radius) + " mi");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        switchNotificationsAnswered = (Switch) findViewById(R.id.switchNotificationsAnswered);
        switchNotificationsQuestions = (Switch) findViewById(R.id.switchNotificationsQuestions);
        if("on".equals(KeyValueDB.get(getApplicationContext(), "newQuestionNotificationOn"))){
            newQuestionNotificationsOn = true;
            switchNotificationsQuestions.setChecked(true);
        }

        if("on".equals(KeyValueDB.get(getApplicationContext(), "newAnswerNotificationOn"))){
            newAnswerNotificationsOn = true;
            switchNotificationsAnswered.setChecked(true);
        }
    }

    public void cancel(View view){
        finish();
    }

    public void save(View view){
        if(switchNotificationsAnswered.isChecked()){
            KeyValueDB.set(getApplicationContext(), "newAnswerNotificationOn", "on");
            startNewAnswerAlarm();
        } else {
            KeyValueDB.set(getApplicationContext(), "newAnswerNotificationOn", "off");
            stopNewAnswerAlarm();
        }

        if(switchNotificationsQuestions.isChecked()){
            KeyValueDB.set(getApplicationContext(), "newQuestionNotificationOn", "on");
            startNewQuestionAlarm();
        } else {
            KeyValueDB.set(getApplicationContext(), "newQuestionNotificationOn", "off");
            stopNewQuestionAlarm();
        }

        KeyValueDB.set(getApplicationContext(), "nearMeRadius", radius);

        finish();
    }

    public void startNewQuestionAlarm() {
        NotificationEventReceiver.setupNewQuestionAlarm(getApplicationContext());
    }

    public void startNewAnswerAlarm() {
        NotificationEventReceiver.setupNewAnswerAlarm(getApplicationContext());
    }

    public void stopNewQuestionAlarm() {
        NotificationEventReceiver.stopNewQuestionAlarm(getApplicationContext());
    }

    public void stopNewAnswerAlarm() {
        NotificationEventReceiver.stopNewAnswerAlarm(getApplicationContext());
    }
}
