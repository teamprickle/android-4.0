package com.prickle4.activities;

/**
 * Created by Drake Lee on 3/25/2016.
 */
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;

import com.prickle4.R;
import com.prickle4.adapters.ArrayAdapterPlaceItem;
import com.prickle4.adapters.PlaceItem;
import com.prickle4.helpers.CustomRequest;
import com.prickle4.static_class.Constants;
import com.prickle4.static_class.Utility;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by Drake Lee on 6/23/2015.
 */
public class RegisterActivity extends Activity  {

    private ProgressDialog prgDialog;
    private EditText usernameET;
    private EditText pwdET;
    private RecyclerView listPlaces;

    private static final String TAG = "RegisterActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        usernameET = (EditText)findViewById(R.id.register_username);
        pwdET = (EditText)findViewById(R.id.register_pwd);

        prgDialog = new ProgressDialog(this);
        prgDialog.setMessage("Please wait...");
        prgDialog.setCancelable(false);


        final ArrayAdapterPlaceItem adapter = new ArrayAdapterPlaceItem(this, new ArrayList<PlaceItem>());
        listPlaces = (RecyclerView) findViewById(R.id.places_list);
        GridLayoutManager layoutManager = new GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false);
        listPlaces.setLayoutManager(layoutManager);
        listPlaces.setAdapter(adapter);
        listPlaces.setItemAnimator(null);

        // Places Auto Complete----

        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.register_autocomplete_fragment);

        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_NONE)
                .build();

        autocompleteFragment.setFilter(typeFilter);

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // Add places here
                ((ArrayAdapterPlaceItem) listPlaces.getAdapter()).addItem(new PlaceItem(place.getName().toString(), place.getLatLng().latitude, place.getLatLng().longitude));
                Log.i(TAG, "Place: " + place.getLatLng().longitude);
            }

            @Override
            public void onError(Status status) {
                Log.i(TAG, "An error occurred: " + status);
            }
        });
    }

    /**
     * Method gets triggered when activity_register is clicked
     * Make sure that all input values are set
     *
     * @param view
     **/
    public void registerUser(View view) {
        String username = usernameET.getText().toString();
        String password = pwdET.getText().toString();
        String passwordHash="";
        try {
            passwordHash = Utility.SHA1(password);
        } catch (NoSuchAlgorithmException no) {

        } catch (UnsupportedEncodingException e) {

        }

        if (Utility.isNotNull(username) && Utility.isNotNull(password)) {
            invokeWS(username, passwordHash);
        } else {
            Toast.makeText(getApplicationContext(), "Please don't leave anything blank", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Perform RESTful webservice
     * @param username, password
     */
    public void invokeWS(final String username, final String password) {

    prgDialog.show();

    Map<String, String> params = new HashMap<String, String>();
    params.put("username", username);
    params.put("password", password);

    JSONObject jsonObject = new JSONObject();
    String info = "";
        //jsonObject.put("locations", ((ArrayAdapterPlaceItem) listPlaces.getAdapter()).data)
    for(int i = 0; i < listPlaces.getAdapter().getItemCount(); ++i) {
        try {
            info = "{\"name\":\"" + ((ArrayAdapterPlaceItem) listPlaces.getAdapter()).getItem(i).getName() +
                    "\", \"lng\":" + ((ArrayAdapterPlaceItem) listPlaces.getAdapter()).getItem(i).getLng() +
                    ", \"lat\":" + ((ArrayAdapterPlaceItem) listPlaces.getAdapter()).getItem(i).getLat() + "}";
            jsonObject.put("place_"+i, info);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    params.put("places", jsonObject.toString());

    CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, Constants.REGISTER_URL, params,
            new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if (response.getString("statusCode").equals("200")) {
                            prgDialog.hide();
                            setDefaultValues();
                            Toast.makeText(getApplicationContext(), "You are registered!", Toast.LENGTH_LONG).show();
                        } else if (response.getString("statusCode").equals("401")) {
                            prgDialog.hide();
                            setDefaultValues();
                            Toast.makeText(getApplicationContext(), "Username taken!", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {

                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    prgDialog.hide();
                    Toast.makeText(getApplicationContext(), "Registration Error!", Toast.LENGTH_LONG).show();
                    System.out.println("Error in user registration.");
                    error.printStackTrace();
                }
    });

    Volley.newRequestQueue(this).add(jsObjRequest);

//    public void navigatetoPhoneActivity(String userEmail) {
//        settings = getSharedPreferences("PeepUserInfo", 0);
//        SharedPreferences.Editor editor = settings.edit();
//        editor.putString("peepEmail", userEmail);
//        editor.commit();
//        Intent phoneIntent = new Intent(getApplicationContext(), PhoneActivity.class);
//        phoneIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        startActivity(phoneIntent);
    }

    public void navigatetoLoginActivity(View view) {
        Intent loginIntent = new Intent(getApplicationContext(), LoginActivity.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(loginIntent);
    }

    /**
     * Set default values for edit view
     */
    public void setDefaultValues() {
        usernameET.setText("");
        pwdET.setText("");
    }

}
