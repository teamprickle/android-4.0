package com.prickle4.activities;

import android.graphics.Bitmap;
import android.media.Image;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.prickle4.R;
import com.prickle4.fragments.QuestionRecyclerViewAdapter;
import com.prickle4.fragments.question_content.QuestionContent;
import com.prickle4.helpers.CustomArrayRequest;
import com.prickle4.helpers.CustomRequest;
import com.prickle4.static_class.Constants;
import com.prickle4.static_class.KeyValueDB;
import com.prickle4.static_class.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ProfileActivity extends AppCompatActivity {

    Bitmap bitmapAvatar;
    ImageView imgAvatar;
    TextView textNumQuestions;
    TextView textNumAnswers;
    TextView textFavoritePlaces;
    TextView textFullName;
    TextView textBio;
    TextView textUsername;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        textUsername = (TextView) findViewById(R.id.textUsername);
        textUsername.setText(KeyValueDB.get(getApplicationContext(), "prickleUsername"));
        textNumQuestions = (TextView) findViewById(R.id.textNumQuestions);
        textNumAnswers = (TextView) findViewById(R.id.textNumAnswers);
        textFavoritePlaces = (TextView) findViewById(R.id.textFavoritePlaces);
        textFullName = (TextView) findViewById(R.id.textFullName);
        textBio = (TextView) findViewById(R.id.textBio);
        imgAvatar = (ImageView) findViewById(R.id.imgAvatar);
        getProfileInformation();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Utility.unbindDrawables(findViewById(R.id.profileRoot));
        System.gc();
    }

    public void getProfileInformation() {
        textUsername.setText(KeyValueDB.get(getApplicationContext(), "prickleUsername"));

        String bio = KeyValueDB.get(getApplicationContext(), "userDescription");
        if (bio != "ERROR") {
            textBio.setText(bio);
        }
        String firstName = KeyValueDB.get(getApplicationContext(), "userFirstName");
        String lastName = KeyValueDB.get(getApplicationContext(), "userLastName");
        StringBuilder fullName = new StringBuilder();
        if (firstName != "ERROR") {
            fullName.append(firstName);
        }

        if (lastName != "ERROR") {
            fullName.append(" ");
            fullName.append(lastName);
        }
        textFullName.setText(fullName);

        String avatarBase64 = KeyValueDB.get(getApplicationContext(), "userAvatar");
        if (avatarBase64 != "ERROR") {
            bitmapAvatar = Utility.decodeBase64(avatarBase64);
            imgAvatar.setImageBitmap(bitmapAvatar);
        }

        //Need to get favorite locations
    }

    public void getNumQuestionsAnswers() {
        Map<String, String> params = new HashMap<String, String>();

        CustomRequest jsRequest = new CustomRequest(Request.Method.GET, Constants.PROFILE_URL, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            int numQuestions = response.getInt("number_questions");
                            int numAnswers = response.getInt("number_answers");
                            textNumQuestions.setText(String.valueOf(numQuestions));
                            textNumAnswers.setText(String.valueOf(numAnswers));
                        } catch (JSONException e) {
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Error sending profile information!", Toast.LENGTH_LONG).show();
                System.out.println("Error in profile PUT.");
                error.printStackTrace();
            }
        }, this);

        Volley.newRequestQueue(getApplicationContext()).add(jsRequest);
    }
}
