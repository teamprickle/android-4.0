package com.prickle4.static_class;

/**
 * Created by Kevin on 2/29/2016.
 */
public final class Constants {
    public static final String LOCATIONS_URL = "http://52.24.247.1:3456/api/locations";
    public static final String RATE_URL = "http://52.24.247.1:3456/api/rate";
    public static final String REGISTER_URL = "http://52.24.247.1:3456/api/users";
    public static final String LOGIN_URL = "http://52.24.247.1:3456/api/users/validate";
    public static final String QUESTION_URL = "http://52.24.247.1:3456/api/question";
    public static final String USER_QUESTIONS_URL = "http://52.24.247.1:3456/api/questions/user/";
    public static final String NEAR_ME_URL = "http://52.24.247.1:3456/api/questions/nearMe";
    public static final String MAP_URL = "http://52.24.247.1:3456/api/map";
    public static final String ANSWER_URL = "http://52.24.247.1:3456/api/answer";
    public static final String UPVOTE_URL = "http://52.24.247.1:3456/api/upvote/answer";
    public static final String DOWNVOTE_URL = "http://52.24.247.1:3456/api/downvote/answer";
    public static final String PROFILE_URL = "http://52.24.247.1:3456/api/user";
    public static final String UPDATE_PROFILE_URL = "http://52.24.247.1:3456/api/user/updateProfile";
    public static final String NEW_QUESTION_NOTIFICATIONS_URL = "http://52.24.247.1:3456/api/questions/new";
    public static final String NEW_ANSWER_NOTIFICATIONS_URL = "http://52.24.247.1:3456/api/answers/new";

    public static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 0;
    public static final String PLACES_API_KEY = "AIzaSyBS3BHvxUOpz_SzFNbmsc1USgcXRahZXrc";

    public static final int QUESTIONS_NEAR_ME = 0;
    public static final int MY_QUESTIONS = 1;
}
