package com.prickle4.static_class;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Drake Lee on 3/27/2016.
 */
public class KeyValueDB {
    private SharedPreferences sharedPreferences;
    private static String PREF_NAME = "PrickleUserInfo";

    public KeyValueDB() {

    }

    private static SharedPreferences getPrefs(Context context) {
        return context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public static String get(Context context, String key) {
        return getPrefs(context).getString(key, "ERROR");
    }

    public static long getLong(Context context, String key) {
        return getPrefs(context).getLong(key, 0);
    }

    public static int getInt(Context context, String key) {
        return getPrefs(context).getInt(key, 10);
    }

    public static void set(Context context, String key, String value) {
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static void set(Context context, String key, int value) {
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static void set(Context context, String key, long value) {
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.putLong(key, value);
        editor.commit();
    }
}
