package com.prickle4.static_class;

import android.content.Context;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * Created by Kevin on 4/26/2016.
 */
public class DateTimeUtils {

    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;
    private static final int YEAR_MILLIS = 365 * DAY_MILLIS;

    public static String getTimeAgo(long time) {
        if (time < 1000000000000L) {
            // if timestamp given in seconds, convert to millis
            time *= 1000;
        }

        Calendar today = new GregorianCalendar();
        // reset hour, minutes, seconds and millis
        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MILLISECOND, 0);
        today.add(Calendar.DAY_OF_MONTH, 1);

        long todayTimeInMillis = today.getTimeInMillis();
        long now = System.currentTimeMillis();
        if (time > todayTimeInMillis || time <= 0) {
            return null;
        }

        // TODO: localize
        final long diffToday = todayTimeInMillis - time;
        final long diffNow = now - time;
        if (diffNow < MINUTE_MILLIS) {
            return "just now";
        } else if (diffNow < 2 * MINUTE_MILLIS) {
            return "a minute ago";
        } else if (diffNow < 50 * MINUTE_MILLIS) {
            return diffNow / MINUTE_MILLIS + " minutes ago";
        } else if (diffNow < 90 * MINUTE_MILLIS) {
            return "an hour ago";
        } else if (diffNow < 24 * HOUR_MILLIS) {
            return diffNow / HOUR_MILLIS + " hours ago";
        } else if (diffToday < 48 * HOUR_MILLIS) {
            Date date = new Date(time);
            SimpleDateFormat sdf = new SimpleDateFormat("'yesterday at' h:mm a");
            return sdf.format(date);
        } else if (diffToday < YEAR_MILLIS){
            Date date = new Date(time);
            SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM d 'at' h:mm a");
            return sdf.format(date);
        } else {
            Date date = new Date(time);
            SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM d yyyy 'at' h:mm a");
            return sdf.format(date);
        }
    }

    public static long getLongFromMongoDB(String dateString) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            Date d = df.parse(dateString);
            return d.getTime();
        } catch (ParseException e) {
            return 0;
        }
    }

    public static String getMongoDBFromLong(long time){
        Date d = new Date(time);
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        return df.format(d);
    }

    public static String formattedTimeFromMongoDB(String dateString, Context ctx){
        long time = getLongFromMongoDB(dateString);
        if(time == 0) return "-";
        return getTimeAgo(time);
    }

    // date format
    public static String formatDate(String dateString) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            Date date = df.parse(dateString);
            SimpleDateFormat sdf = new SimpleDateFormat("EEEE, M-dd-yyyy 'at' h:mm a");
            return sdf.format(date);
        }
        catch (Exception e) {
            System.out.println("Parse Exception in formatDate");
        }
        return dateString;
    }
}
