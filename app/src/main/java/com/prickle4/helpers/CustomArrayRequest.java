package com.prickle4.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.HttpHeaderParser;
import com.prickle4.static_class.KeyValueDB;

public class CustomArrayRequest extends Request<JSONArray> {

    private Listener<JSONArray> listener;
    private Map<String, String> params;
    public Context context;

    public CustomArrayRequest(String url, Map<String, String> params,
                         Listener<JSONArray> responseListener, ErrorListener errorListener) {
        super(Method.GET, url, errorListener);
        this.listener = responseListener;
        this.params = params;
    }

    public CustomArrayRequest(int method, String url, Map<String, String> params,
                         Listener<JSONArray> reponseListener, ErrorListener errorListener) {
        super(method, url, errorListener);
        this.listener = reponseListener;
        this.params = params;
    }

    public CustomArrayRequest(int method, String url, Map<String, String> params,
                         Listener<JSONArray> reponseListener, ErrorListener errorListener, Context context) {
        super(method, url, errorListener);
        this.listener = reponseListener;
        this.params = params;
        this.context = context;
    }

    protected Map<String, String> getParams()
            throws com.android.volley.AuthFailureError {
        return params;
    }

    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> params = new HashMap<String, String>();
        params.put("Content-Type", "application/x-www-form-urlencoded");
        String username = "ERROR";
        String password = "ERROR";
        if (context != null) {
            username = KeyValueDB.get(context, "prickleUsername");
            password = KeyValueDB.get(context, "pricklePassword");
        }

        if (this.params.get("username") != null && this.params.get("password") != null) {
            String credentials = String.format("%s:%s", this.params.get("username"), this.params.get("password"));
            String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.DEFAULT);
            params.put("Authorization", auth);
        } else if (!username.equals("ERROR") && !password.equals("ERROR")) {
            String credentials = String.format("%s:%s", username, password);
            String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.DEFAULT);
            params.put("Authorization", auth);
        }
        return params;
    }

    @Override
    protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers));
            return Response.success(new JSONArray(jsonString),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JSONException je) {
            return Response.error(new ParseError(je));
        }
    }

    @Override
    protected void deliverResponse(JSONArray response) {
        // TODO Auto-generated method stub
        listener.onResponse(response);
    }
}