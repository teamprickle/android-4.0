package com.prickle4.helpers.notifications;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.prickle4.R;
import com.prickle4.activities.question.QuestionViewActivity;
import com.prickle4.static_class.KeyValueDB;

/**
 * Created by Kevin on 4/26/2016.
 */
public class NotificationIntentService extends IntentService {

    private static final int NOTIFICATION_ID = 1;
    private static final String ACTION_START = "ACTION_START";
    private static final String ACTION_DELETE = "ACTION_DELETE";
    private static final String ACTION_NEW_QUESTION_NEAR_ME = "ACTION_NEW_QUESTION_NEAR_ME";
    private static final String ACTION_NEW_ANSWER_FOR_ME = "ACTION_NEW_ANSWER_FOR_ME";

    public NotificationIntentService() {
        super(NotificationIntentService.class.getSimpleName());
    }

    public static Intent createIntentStartQuestionNotificationService(Context context, String questionId) {
        Intent intent = new Intent(context, NotificationIntentService.class);
        intent.putExtra("questionId", questionId);
        intent.setAction(ACTION_NEW_QUESTION_NEAR_ME);
        KeyValueDB.set(context, "lastNewQuestionFound", System.currentTimeMillis());
        return intent;
    }

    public static Intent createIntentStartAnswerNotificationService(Context context, String questionId) {
        Intent intent = new Intent(context, NotificationIntentService.class);
        intent.putExtra("questionId", questionId);
        intent.setAction(ACTION_NEW_ANSWER_FOR_ME);
        KeyValueDB.set(context, "lastMyQuestionAnswered", System.currentTimeMillis());
        return intent;
    }

    public static Intent createIntentDeleteNotification(Context context) {
        Intent intent = new Intent(context, NotificationIntentService.class);
        intent.setAction(ACTION_DELETE);
        return intent;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d(getClass().getSimpleName(), "onHandleIntent, started handling a notification event");
        try {
            String action = intent.getAction();
            String questionId = intent.getStringExtra("questionId");

            if (ACTION_NEW_ANSWER_FOR_ME.equals(action)) {
                processStartAnsweredQuestionNotification(questionId);
            }
            if (ACTION_NEW_QUESTION_NEAR_ME.equals(action)) {
                processStartAskedQuestionNotification(questionId);
            }
        } finally {
            WakefulBroadcastReceiver.completeWakefulIntent(intent);
        }
    }

    private void processDeleteNotification(Intent intent) {
        // Log something?
    }

    private void processStartAskedQuestionNotification(String questionId) {
        // Do something. For example, fetch fresh data from backend to create a rich notification?

        final NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setContentTitle("Prickle")
                .setAutoCancel(true)
                .setColor(getResources().getColor(R.color.color_medium))
                .setContentText("Somebody asked a question nearby!")
                .setSmallIcon(R.drawable.ic_question_answer);
        Intent questionIntent = new Intent(this, QuestionViewActivity.class);
        questionIntent.putExtra("questionId", questionId);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                NOTIFICATION_ID,
                questionIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);
        builder.setDeleteIntent(NotificationEventReceiver.getDeleteIntent(this));

        final NotificationManager manager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(NOTIFICATION_ID, builder.build());
    }

    private void processStartAnsweredQuestionNotification(String questionId) {
        // Do something. For example, fetch fresh data from backend to create a rich notification?

        final NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setContentTitle("Prickle")
                .setAutoCancel(true)
                .setColor(getResources().getColor(R.color.color_medium))
                .setContentText("Somebody answered your question!")
                .setSmallIcon(R.drawable.ic_question_answer);
        Intent questionIntent = new Intent(this, QuestionViewActivity.class);
        questionIntent.putExtra("questionId", questionId);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                NOTIFICATION_ID,
                questionIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);
        builder.setDeleteIntent(NotificationEventReceiver.getDeleteIntent(this));

        final NotificationManager manager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(NOTIFICATION_ID, builder.build());
    }
}
