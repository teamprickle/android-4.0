package com.prickle4.helpers.notifications;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.prickle4.helpers.CustomRequest;
import com.prickle4.static_class.Constants;
import com.prickle4.static_class.DateTimeUtils;
import com.prickle4.static_class.KeyValueDB;
import com.prickle4.static_class.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Kevin on 4/26/2016.
 */
public class NotificationEventReceiver extends WakefulBroadcastReceiver {

    private static final String ACTION_NEW_QUESTION_NEAR_ME = "ACTION_NEW_QUESTION_NEAR_ME";
    private static final String ACTION_NEW_ANSWER_FOR_ME = "ACTION_NEW_ANSWER_FOR_ME";
    private static final String ACTION_START_NOTIFICATION_SERVICE = "ACTION_START_NOTIFICATION_SERVICE";
    private static final String ACTION_DELETE_NOTIFICATION = "ACTION_DELETE_NOTIFICATION";
    private static final long freq = (long) ((1 / 30.0) * AlarmManager.INTERVAL_FIFTEEN_MINUTES);
    static PendingIntent getNewQuestionIntent;
    static PendingIntent getNewAnswerIntent;

    public static void setupNewQuestionAlarm(Context context) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        getNewQuestionIntent = getNewQuestionsPendingIntent(context);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, getTriggerAt(new Date()), freq, getNewQuestionIntent);
    }

    public static void stopNewQuestionAlarm(Context context) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        getNewQuestionIntent = getNewQuestionsPendingIntent(context);
        alarmManager.cancel(getNewQuestionIntent);
    }

    public static void setupNewAnswerAlarm(Context context){
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        getNewAnswerIntent = getNewAnswerPendingIntent(context);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, getTriggerAt(new Date()), freq, getNewAnswerIntent);
    }

    public static void stopNewAnswerAlarm(Context context){
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        getNewAnswerIntent = getNewAnswerPendingIntent(context);
        alarmManager.cancel(getNewAnswerIntent);
    }

    public static void getNewQuestionsNearMe(final Context context, double lat, double lng) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("lat", String.valueOf(lat));
        params.put("lng", String.valueOf(lng));
        params.put("last", DateTimeUtils.getMongoDBFromLong(KeyValueDB.getLong(context, "lastNewQuestionFound")));
        params.put("radius", String.valueOf(Utility.milesToLatLng(KeyValueDB.getInt(context, "nearMeRadius"))));
        CustomRequest jsRequest = new CustomRequest(Request.Method.PUT, Constants.NEW_QUESTION_NOTIFICATIONS_URL, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        System.out.println(response);
                        try {
                            boolean newQuestion = response.getBoolean("new");
                            if (newQuestion) {
                                JSONObject question = response.getJSONObject("question");
                                String questionId = question.getString("_id");
                                Intent serviceIntent = null;
                                Log.i(getClass().getSimpleName(), "onReceive from alarm, starting notification service");
                                serviceIntent = NotificationIntentService.createIntentStartQuestionNotificationService(context, questionId);
                                if (serviceIntent != null) {
                                    startWakefulService(context, serviceIntent);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "Error in getting questions near you!", Toast.LENGTH_LONG).show();
                System.out.println("Error in questions GET.");
                error.printStackTrace();
            }
        }, context);

        Volley.newRequestQueue(context).add(jsRequest);
    }

    public static void getMyQuestionsAnswered(final Context context) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("last", DateTimeUtils.getMongoDBFromLong(KeyValueDB.getLong(context, "lastMyQuestionAnswered")));
        CustomRequest jsRequest = new CustomRequest(Request.Method.PUT, Constants.NEW_ANSWER_NOTIFICATIONS_URL, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        System.out.println(response);
                        try {
                            boolean newQuestion = response.getBoolean("new");
                            if (newQuestion) {
                                JSONObject answer = response.getJSONObject("answer");
                                String questionId = answer.getString("questionId");
                                Intent serviceIntent = null;
                                Log.i(getClass().getSimpleName(), "onReceive from alarm, starting notification service");
                                serviceIntent = NotificationIntentService.createIntentStartAnswerNotificationService(context, questionId);
                                if (serviceIntent != null) {
                                    startWakefulService(context, serviceIntent);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "Error in getting questions near you!", Toast.LENGTH_LONG).show();
                System.out.println("Error in questions GET.");
                error.printStackTrace();
            }
        }, context);

        Volley.newRequestQueue(context).add(jsRequest);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        System.out.println("This should print every minute.");
        String action = intent.getAction();
        if(ACTION_NEW_ANSWER_FOR_ME.equals(action)){
            getMyQuestionsAnswered(context);
        } else if (ACTION_NEW_QUESTION_NEAR_ME.equals(action)){
            double lat = 38.64564;
            double lng = -90.3145963;
            if (!KeyValueDB.get(context, "lat").equals("ERROR")) {
                lat = Double.parseDouble(KeyValueDB.get(context, "lat"));
                lng = Double.parseDouble(KeyValueDB.get(context, "lng"));
            }
            getNewQuestionsNearMe(context, lat, lng);
        }
    }

    private static long getTriggerAt(Date now) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(now);
        return calendar.getTimeInMillis();
    }


    private static PendingIntent getNewQuestionsPendingIntent(Context context) {
        Intent intent = new Intent(context, NotificationEventReceiver.class);
        intent.setAction(ACTION_NEW_QUESTION_NEAR_ME);
        return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private static PendingIntent getNewAnswerPendingIntent(Context context) {
        Intent intent = new Intent(context, NotificationEventReceiver.class);
        intent.setAction(ACTION_NEW_ANSWER_FOR_ME);
        return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private static PendingIntent getStartPendingIntent(Context context) {
        Intent intent = new Intent(context, NotificationEventReceiver.class);
        intent.setAction(ACTION_START_NOTIFICATION_SERVICE);
        return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public static PendingIntent getDeleteIntent(Context context) {
        Intent intent = new Intent(context, NotificationEventReceiver.class);
        intent.setAction(ACTION_DELETE_NOTIFICATION);
        return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

}