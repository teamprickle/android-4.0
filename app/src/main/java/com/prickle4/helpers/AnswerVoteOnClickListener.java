package com.prickle4.helpers;

import android.content.Context;
import android.util.StringBuilderPrinter;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.prickle4.R;
import com.prickle4.static_class.Constants;
import com.prickle4.static_class.KeyValueDB;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Kevin on 4/23/2016.
 */
public class AnswerVoteOnClickListener implements View.OnClickListener {

    public String answerId;
    public Context mContext;

    public AnswerVoteOnClickListener(String answerId, Context context) {
        this.answerId = answerId;
        mContext = context;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.upvote_answer_card_button:
                upVote(answerId);
                break;
            case R.id.downvote_answer_card_button:
                downVote(answerId);
                break;
            default:
                break;
        }
    }

    public void upVote(String answerId) {
        Map<String, String> params = new HashMap<>();
        params.put("username", KeyValueDB.get(mContext, "prickleUsername"));
        params.put("userId", KeyValueDB.get(mContext, "prickleUserId"));
        params.put("type", "true");
        params.put("answerId", answerId);

        CustomRequest jsObjRequest = new CustomRequest(Request.Method.PUT, Constants.UPVOTE_URL + "/" + answerId, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            System.out.println(response);
                            if (response.get("statusCode").equals("200")) {
                                Toast.makeText(mContext.getApplicationContext(), "Answer upvoted!", Toast.LENGTH_LONG).show();
                            } else if (response.get("statusCode").equals("187")) {
                                Toast.makeText(mContext.getApplicationContext(), "Can't upvote an answer more than once!", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mContext.getApplicationContext(), "Error upvoting answer!", Toast.LENGTH_LONG).show();
                System.out.println("Error in answer POST.");
                error.printStackTrace();
            }
        }, mContext);

        Volley.newRequestQueue(mContext.getApplicationContext()).add(jsObjRequest);
    }

    public void downVote(String answerId) {
        Map<String, String> params = new HashMap<>();
        params.put("username", KeyValueDB.get(mContext, "prickleUsername"));
        params.put("userId", KeyValueDB.get(mContext, "prickleUserId"));
        params.put("type", "false");
        params.put("answerId", answerId);

        CustomRequest jsObjRequest = new CustomRequest(Request.Method.PUT, Constants.DOWNVOTE_URL + "/" + answerId, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            System.out.println(response);
                            if (response.get("statusCode").equals("200")) {
                                Toast.makeText(mContext.getApplicationContext(), "Answer downvoted!", Toast.LENGTH_LONG).show();
                            } else if (response.get("statusCode").equals("187")) {
                                Toast.makeText(mContext.getApplicationContext(), "Can't downvote an answer more than once!", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mContext.getApplicationContext(), "Error downvoting answer!", Toast.LENGTH_LONG).show();
                System.out.println("Error in answer POST.");
                error.printStackTrace();
            }
        }, mContext);

        Volley.newRequestQueue(mContext.getApplicationContext()).add(jsObjRequest);
    }


}
