package com.prickle4.fragments;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.prickle4.R;
import com.prickle4.fragments.question_content.QuestionContent;
import com.prickle4.fragments.question_content.QuestionContent.Question;
import com.prickle4.helpers.CustomArrayRequest;
import com.prickle4.static_class.Constants;
import com.prickle4.static_class.DateTimeUtils;
import com.prickle4.static_class.KeyValueDB;
import com.prickle4.static_class.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A fragment representing a list of Items.
 * <p>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class QuestionListFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    private static final String ARG_LIST_TYPE = "list-type";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private int mListType = Constants.QUESTIONS_NEAR_ME;
    private OnListFragmentInteractionListener mListener;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeContainer;
    private LocationManager lm;
    private Double lat;
    private Double lng;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public QuestionListFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static QuestionListFragment newInstance(int columnCount, int listType) {
        QuestionListFragment fragment = new QuestionListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        args.putInt(ARG_LIST_TYPE, listType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
            mListType = getArguments().getInt(ARG_LIST_TYPE);

        }

        lm = (LocationManager) getActivity().getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        try {
            lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 60000 * 60, 90, mLocationListener);
        } catch (SecurityException e) {

        }

        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        if (gps_enabled || network_enabled) {
            Location location = getLastKnownLocation();
            if (location != null) {
                lat = location.getLatitude();
                lng = location.getLongitude();
            }
        }
    }

    private Location getLastKnownLocation() {
        List<String> providers = lm.getProviders(true);
        Location bestLocation = null;
        try {
            for (String provider : providers) {
                Location l = lm.getLastKnownLocation(provider);

                if (l == null) {
                    continue;
                }
                if (bestLocation == null
                        || l.getAccuracy() < bestLocation.getAccuracy()) {
                    bestLocation = l;
                }
            }
        } catch (SecurityException e) {

        }
        if (bestLocation == null) {
            return null;
        }
        KeyValueDB.set(getContext(), "lng", String.valueOf(bestLocation.getLongitude()));
        KeyValueDB.set(getContext(), "lat", String.valueOf(bestLocation.getLatitude()));
        return bestLocation;
    }

    ;

    public void getMyQuestions(final QuestionContent content, final Fragment fragment) {
        Map<String, String> params = new HashMap<String, String>();

        CustomArrayRequest jsArrRequest = new CustomArrayRequest(Request.Method.GET, Constants.USER_QUESTIONS_URL, params,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            int numQuestions = response.length();
                            for (int i = 0; i < numQuestions; i++) {
                                JSONObject question = response.getJSONObject(i);
                                String locationName = "";
                                try {
                                    locationName = question.getJSONObject("place").getString("name");
                                } catch (JSONException e) {
                                }
                                content.addItem(QuestionContent.createQuestion(question.getString("_id"), question.getString("title"), question.getString("details"), question.getString("username"), locationName, DateTimeUtils.formattedTimeFromMongoDB(question.getString("created"), getContext()), null, null, question.getInt("views")));
                                recyclerView.setAdapter(new QuestionRecyclerViewAdapter(content.ITEMS, mListener));
                                recyclerView.getAdapter().notifyDataSetChanged();
                                swipeContainer.setRefreshing(false);
                            }
                        } catch (JSONException e) {
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(fragment.getActivity().getApplicationContext(), "Error getting your asked questions!", Toast.LENGTH_LONG).show();
                System.out.println("Error in questions GET.");
                error.printStackTrace();
            }
        }, fragment.getActivity());

        Volley.newRequestQueue(fragment.getActivity().getApplicationContext()).add(jsArrRequest);
    }

    public void getQuestionsNearMe(final QuestionContent content, final Fragment fragment) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("radius", String.valueOf(Utility.milesToLatLng(KeyValueDB.getInt(getContext(), "nearMeRadius"))));

        // TODO: FOR TESTING ONLY. IF LNG AND LAT CAN'T BE ASSIGNED BY GPS MANUALLY ASSIGN
        if (lng == null || lat == null) {
            lng = -90.3145963;
            lat = 38.64564;
        }

        CustomArrayRequest jsArrRequest = new CustomArrayRequest(Request.Method.PUT, Constants.NEAR_ME_URL + "?lng=" + lng.toString() +
                "&lat=" + lat.toString(), params,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            int numQuestions = response.length();
                            for (int i = 0; i < numQuestions; i++) {
                                JSONObject question = response.getJSONObject(i);
                                String locationName = "";
                                try {
                                    locationName = question.getJSONObject("place").getString("name");
                                } catch (JSONException e) {
                                }
                                content.addItem(QuestionContent.createQuestion(question.getString("_id"), question.getString("title"), question.getString("details"), question.getString("username"), locationName, DateTimeUtils.formattedTimeFromMongoDB(question.getString("created"), getContext()), null, null, question.getInt("views")));
                                recyclerView.setAdapter(new QuestionRecyclerViewAdapter(content.ITEMS, mListener));
                                recyclerView.getAdapter().notifyDataSetChanged();
                                swipeContainer.setRefreshing(false);
                            }
                        } catch (JSONException e) {

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(fragment.getActivity().getApplicationContext(), "Error in getting questions near you!", Toast.LENGTH_LONG).show();
                System.out.println("Error in questions GET.");
                error.printStackTrace();
            }
        }, fragment.getActivity());

        Volley.newRequestQueue(fragment.getActivity().getApplicationContext()).add(jsArrRequest);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_question_list, container, false);
        swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainer);

        // Setup refresh listener which triggers new data loading

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                switch (mListType) {
                    case Constants.QUESTIONS_NEAR_ME: {
                        QuestionContent content = new QuestionContent();
                        getQuestionsNearMe(content, QuestionListFragment.this);
                        break;
                    }
                    case Constants.MY_QUESTIONS: {
                        QuestionContent content = new QuestionContent();
                        getMyQuestions(content, QuestionListFragment.this);
                        break;
                    }
                }
            }
        });

        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        // Set the adapter
        recyclerView = (RecyclerView) view.findViewById(R.id.list);
        switch (mListType) {
            case Constants.QUESTIONS_NEAR_ME: {
                QuestionContent content = new QuestionContent();
                getQuestionsNearMe(content, this);
                break;
            }
            case Constants.MY_QUESTIONS: {
                QuestionContent content = new QuestionContent();
                getMyQuestions(content, this);
                break;
            }
        }
        Context context = view.getContext();

        if (mColumnCount <= 1) {
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
        }

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(Question item);
    }

    private final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {
            lng = location.getLongitude();
            lat = location.getLatitude();
        }

        @Override
        public void onProviderDisabled(String provider) {

        }

        @Override
        public void onProviderEnabled(String provider) {
            try {
                Location location = lm.getLastKnownLocation(provider);
                lng = location.getLongitude();
                lat = location.getLatitude();
            } catch (SecurityException e) {

            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }
    };
}
