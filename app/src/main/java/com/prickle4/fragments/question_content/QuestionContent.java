package com.prickle4.fragments.question_content;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample title for user interfaces created by
 * Android template wizards.
 * <p/>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class QuestionContent {

    public final List<Question> ITEMS;
    public final Map<String, Question> ITEM_MAP;

    public QuestionContent(){
        ITEMS = new ArrayList<Question>();
        ITEM_MAP = new HashMap<String, Question>();
    }

    public void addItem(Question item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }

    public static Question createQuestion(String id, String title, String details, String username, String location, String timeCreated, String base64avatar, String userDescription, int views) {
        return new Question(id, title, details, username, location, timeCreated, base64avatar, userDescription, views);
    }

    public static class Question {
        public final String id;
        public final String title;
        public final String details;
        public final String username;
        public final String location;
        public final String timeCreated;
        public final String base64avatar;
        public final String userDescription;
        public final int views;

        public Question(String id, String title, String details, String username, String location, String timeCreated, String base64avatar, String userDescription, int views) {
            this.id = id;
            this.title = title;
            this.details = details;
            this.username = username;
            this.location = location;
            this.timeCreated = timeCreated;
            this.base64avatar = base64avatar;
            this.userDescription = userDescription;
            this.views = views;
        }

        @Override
        public String toString() {
            return title;
        }
    }
}
