package com.prickle4.fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.prickle4.R;
import com.prickle4.activities.question.QuestionViewActivity;
import com.prickle4.helpers.CustomArrayRequest;
import com.prickle4.static_class.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ExploreFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ExploreFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ExploreFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    MapView mMapView;
    private GoogleMap mMap;
    private LocationManager lm;
    private double lat;
    private double lng;
    private HashMap<Marker, String> mapMarkerToId;
    private HashMap<String, Marker> mapIdToMarker;

    private OnFragmentInteractionListener mListener;

    public ExploreFragment() {}

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ExploreFragment.
     */
    public static ExploreFragment newInstance(int one) {
        ExploreFragment fragment = new ExploreFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_explore, container, false);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mMapView = (MapView) v.findViewById(R.id.map);
        mMapView.getMapAsync(this);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();
        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private Location getLastKnownLocation() {
        List<String> providers = lm.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                Location l = lm.getLastKnownLocation(provider);

                if (l == null) {
                    continue;
                }
                if (bestLocation == null
                        || l.getAccuracy() < bestLocation.getAccuracy()) {
                    bestLocation = l;
                }
            }
        }
        if (bestLocation == null) {
            return null;
        }
        return bestLocation;
    };


    public void getQuestionData() {
        mapMarkerToId = new HashMap<Marker, String>();
        mapIdToMarker = new HashMap<String, Marker>();
        Map<String, String> params = new HashMap<String, String>();
        CustomArrayRequest jsArrRequest = new CustomArrayRequest(Request.Method.GET, Constants.QUESTION_URL, params,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            int numQuestions = response.length();
                            for (int i = 0; i < numQuestions; i++) {
                                JSONObject question = response.getJSONObject(i);
                                String questionTitle = question.getString("title");
                                String id = question.getString("_id");
                                JSONObject place = question.getJSONObject("place");
                                String locName = "";
                                try {
                                    locName = place.getString("name");
                                } catch(JSONException e){}
                                double lat = place.getJSONArray("loc").getDouble(1);
                                double lng = place.getJSONArray("loc").getDouble(0);
                                if((Math.abs(lat - 0.0) > 0.0001) && (Math.abs(lng - 0.0) > 0.0001)) {
                                    LatLng locGPS = new LatLng(lat, lng);
                                    Marker qMarker = mMap.addMarker(new MarkerOptions().position(locGPS).title(questionTitle).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET)));
                                    mapMarkerToId.put(qMarker, id);
                                    mapIdToMarker.put(id, qMarker);
                                }
                            }
                        } catch (JSONException e) {

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ExploreFragment.this.getActivity().getApplicationContext(), "Error retrieving questions!", Toast.LENGTH_LONG).show();
                System.out.println("Error retrieving questions!");
                error.printStackTrace();
            }
        }, ExploreFragment.this.getActivity());

        Volley.newRequestQueue(this.getActivity().getApplicationContext()).add(jsArrRequest);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Constants.MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
    public boolean onMarkerClick(Marker marker) {
        final String questionId = mapMarkerToId.get(marker);
        if(questionId != null) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            ViewGroup container = (ViewGroup) layoutInflater.inflate(R.layout.marker_popup, null);
            TextView textQuestion = (TextView) container.findViewById(R.id.textQuestion);
            textQuestion.setText(marker.getTitle());
            DisplayMetrics dm = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
            int width = dm.widthPixels;
            int height = dm.heightPixels;
            final PopupWindow questionPopup = new PopupWindow(container, (int) (width*0.8), (int) (height*0.4), true);
            questionPopup.setBackgroundDrawable(new ColorDrawable());
            questionPopup.setAnimationStyle(R.style.FadeZoom);
            questionPopup.setOutsideTouchable(true);
            questionPopup.setTouchInterceptor(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View vi, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                        questionPopup.dismiss();
                        return true;
                    }
                    return false;
                }
            });
            Button viewQuestion = (Button) container.findViewById(R.id.buttonViewQuestion);
            viewQuestion.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context context = mMapView.getContext();
                    Intent i = new Intent(context, QuestionViewActivity.class);
                    i.putExtra("questionId", questionId);
                    context.startActivity(i);
                }
            });
            FrameLayout contentFrame = (FrameLayout) getActivity().findViewById(R.id.content_frame);
            questionPopup.showAtLocation(contentFrame, Gravity.CENTER, 0, 0);
        }
        return true;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
            lm = (LocationManager) getActivity().getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
            boolean gps_enabled = false;
            boolean network_enabled = false;

            try {
                gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
            } catch(Exception ex) {}

            try {
                network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            } catch(Exception ex) {}

            if(gps_enabled || network_enabled) {
                Criteria criteria = new Criteria();
                String provider = lm.getBestProvider(criteria, false);
                Location myLocation = getLastKnownLocation();

                lat = myLocation.getLatitude();
                lng = myLocation.getLongitude();

                LatLng coordinate = new LatLng(lat, lng);
                mMap.moveCamera(CameraUpdateFactory.newLatLng(coordinate));
                mMap.moveCamera(CameraUpdateFactory.zoomTo(16));
                mMap.setOnMarkerClickListener(ExploreFragment.this);
                getQuestionData();
            }
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    Constants.MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
