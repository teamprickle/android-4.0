package com.prickle4.fragments;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.prickle4.R;
import com.prickle4.activities.question.QuestionViewActivity;
import com.prickle4.fragments.QuestionListFragment.OnListFragmentInteractionListener;
import com.prickle4.fragments.question_content.QuestionContent.Question;

import org.w3c.dom.Text;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Question} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class QuestionRecyclerViewAdapter extends RecyclerView.Adapter<QuestionRecyclerViewAdapter.ViewHolder> {

    private final List<Question> mValues;
    private final OnListFragmentInteractionListener mListener;

    public QuestionRecyclerViewAdapter(List<Question> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_question, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mQuestionViewsView.setText(String.valueOf(mValues.get(position).views));
        holder.mQuestionTitleView.setText(mValues.get(position).title);
        holder.mQuestionDetailView.setText(mValues.get(position).details);
        holder.mQuestionCreatedView.setText(mValues.get(position).timeCreated);
        holder.mQuestionUserView.setText(mValues.get(position).username);
        if(mValues.get(position).location != ""){
            holder.mQuestionLocationHolder.setVisibility(View.VISIBLE);
            holder.mQuestionLocationView.setText(mValues.get(position).location);
        } else {
            holder.mQuestionLocationHolder.setVisibility(View.GONE);
        }


        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public final View mView;
        public final TextView mQuestionViewsView;
        public final TextView mQuestionTitleView;
        public final TextView mQuestionDetailView;
        public final TextView mQuestionLocationView;
        public final TextView mQuestionCreatedView;
        public final TextView mQuestionUserView;
        //public final TextView mUserBioView;
        public final ImageView mUserAvatarView;
        public final LinearLayout mQuestionLocationHolder;

        public Question mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mQuestionViewsView = (TextView) view.findViewById(R.id.questionViews);
            mQuestionTitleView = (TextView) view.findViewById(R.id.questionTitle);
            mQuestionDetailView = (TextView) view.findViewById(R.id.questionDetail);
            mQuestionUserView = (TextView) view.findViewById(R.id.userName);
            mQuestionCreatedView = (TextView) view.findViewById(R.id.questionCreated);
            mQuestionLocationView = (TextView) view.findViewById(R.id.questionLocation);
            mQuestionLocationHolder = (LinearLayout) view.findViewById(R.id.questionLocationHolder);
            //mUserBioView = (TextView) view.findViewById(R.id.userBio);
            mUserAvatarView = (ImageView) view.findViewById(R.id.userImage);
            mView.setOnClickListener(this);
            mQuestionViewsView.setOnClickListener(this);
            mQuestionTitleView.setOnClickListener(this);
            mQuestionDetailView.setOnClickListener(this);
            mQuestionUserView.setOnClickListener(this);
            mQuestionLocationView.setOnClickListener(this);
            mQuestionLocationHolder.setOnClickListener(this);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mQuestionTitleView.getText() + "'";
        }

        @Override
        public void onClick(View v) {
            Context context = v.getContext();
            Intent i = new Intent(context, QuestionViewActivity.class);
            i.putExtra("questionId", mItem.id);
            context.startActivity(i);
        }
    }
}
